

// In JS, classes can be created using the "class" keyword and {}


/*
Naming convenion for classe: Begin with Uppercase characters
	syntax:
	class NameOfClass {
	}
*/

class Student {

	constructor(name, email, grades) {
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;
		this.passed = "";
		this.passedWithHonors = "";

//Activity
		if (grades.length === 4){
			if(grades.every(grade => grade >= 0 && grade <= 100)){
			this.grades = grades;				
			} else {
				this.grades = undefined
			}
		} else {
			this.grades = undefined
		}
	
	}

	// Methods
	login() {
		console.log(`${this.email} has logged in`)
		return this;
	}

	logout() {
		console.log(`${this.email} has logged out`)
		return this;
	}

	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}.`)
		return this;
	}

	computeAve() {
		let sum = 0; 
		this.grades.forEach(grade => sum = sum + grade)
		// update property
		this.gradeAve = sum / 4;
		// return object
		return this;
	}

	willPass () {
		if (this.computeAve() >= 85) {
			true
		} else {
			false
		}
		return this
	}

	willPassWithHonors(){
        if (this.willPass()) {
        	if (this.computeAve() >= 90){
            true
        } else {
        	false
        }
        } else {
            undefined
    	}
    	return this
	}
}

let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

// Mini Activity

class Person {

	constructor(name, age, nationality, address) {
		this.name = name;
		if (typeof age === "number" && age >= 18) {
			this.age = age;
		} else {
			this.age = undefined;
		};
		this.nationality = nationality;
		this.address = address;
	}
}

let personOne = new Person('Joe', 18, 'Filipino', 'PH')
let personTwo = new Person('Jane', 17, 'Filipino', 'PH')

/*
QUIZ part 1

1. class NameOfClass {}
2. Pascalcase
3. new
4. instantiate
5. constructor

QUIZ part 2

1. N
2. N
3. Y
4. Getter and Setter
5. this

*/

//Getter and Setter
// Best practice dictated that we regulate access to such properties.
// Geter -retrieval
// setter - manipulation


